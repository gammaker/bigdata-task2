val data = readCsv(sqlContext, schema, "/metrics/part-m-00000") //Читаем результат импорта sqoop'ом
val metrics = readCsv(sqlContext, metricsSchema, "file:///media/sf_BigData/HW2/metrics.csv") //Читаем таблицу соответствия id метрик масштабам усреднения, нужно указывать полный путь
val resultDF = aggregate(data, metrics)
println("Result record count: " + resultDF.count) //Убедимся, что после агрегации элементов стало во много раз меньше
saveCsv(resultDF, "/metrics/results") //Сохраняем результат в HDFS
