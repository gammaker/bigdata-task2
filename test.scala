def testAggregate(sc: SparkContext, sqlContext: SQLContext) {
	val testData = Seq(
		Row(1, 0L, 133),
		Row(3, 40000L, 161),
		Row(2, 15000L, 125),
		Row(2, 450000L, 19),
		Row(2, 75000L, 1),
		Row(3, 800000L, 11),
		Row(3, 1400000L, 55)
	)
	val testDF = sqlContext.createDataFrame(sc.parallelize(testData), schema)
	println("Test input data:");
	testDF.show
	
	val testMetrics = Seq(
		Row(1, 1),
		Row(2, 5),
		Row(3, 15)
	)
	val testMetricsDF = sqlContext.createDataFrame(sc.parallelize(testMetrics), metricsSchema)
	println("Test metrics:");
	testMetricsDF.show
	
	println("Expected result:");
	val expectedData = Seq(
		Row(1, 0L, "1m", 133),
		Row(2, 0L, "5m", 63),
		Row(2, 300000L, "5m", 19),
		Row(3, 0L, "15m", 86),
		Row(3, 900000L, "15m", 55)
	)
	val expectedSchema = StructType(Array(
		StructField("id", IntegerType, true),
		StructField("timestamp", LongType, true),
		StructField("scale", StringType, true),
		StructField("value", IntegerType, true)
	))
	val expectedDF = sqlContext.createDataFrame(sc.parallelize(expectedData), expectedSchema)
	expectedDF.show
	
	println("Actual result:");
	val resultDF = aggregate(testDF, testMetricsDF)
	resultDF.show
	
	try {
		assert(resultDF.schema.equals(expectedDF.schema))
		assert(resultDF.collect().sameElements(expectedDF.collect()))
		println("Test passed!");
	}
	catch { case _: Throwable => println("Test failed!");}
}

testAggregate(sc, sqlContext)