import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{StructType, StructField, StringType, IntegerType, LongType}
import org.apache.spark.sql._

//Схема входных данных
val schema = StructType(Array(
	StructField("id", IntegerType, true),
	StructField("timestamp", LongType, true),
	StructField("value", IntegerType, true)
))

//Схема метрики
val metricsSchema = StructType(Array(
	StructField("id", IntegerType, true),
	StructField("scale", IntegerType, true) //Интервал времени в минутах, в который производится усреднение
))

//Эта функция реализует всю требуемую заданием бизнес-логику
//На вход поступает таблица исходных данных и таблица соответствия id метрики <=> масштаб усреднения в минутах.
//На выходе - таблица с агрегированной метрикой в соответствии с заданием
def aggregate(data: DataFrame, metrics: DataFrame): DataFrame = {
	//Добавляем столбец строк, составленных как scale + "m"
	val metricsWithScaleStr = metrics.withColumn("scaleStr", udf( (x: String) => (x + "m")).apply(col("scale")) )
			
	return data.join(metrics, "id") //Каждому id метрики ставится в соответствие scale
		.groupBy($"id", (floor($"timestamp"/($"scale"*60000))*($"scale"*60000)).alias("timestamp")) //Округляем timestamp к низу с соответствующей scale точностью и группируем по id и округлённому timestamp
		.agg(avg("value").cast(IntegerType).alias("value")) //Усредняем значения в пределах выделенных групп
		.join(metricsWithScaleStr, "id") //Присоединяем к результатам строку, описывающую масштаб
		.select($"id", $"timestamp", metricsWithScaleStr("scaleStr").alias("scale"), $"value") //Получаем поля в порядке и формате, соответствующими заданию
}

//Создать DataFrame из файла в формате CSV.
def readCsv(sqlContext: SQLContext, schema: StructType, path: String): DataFrame = {
	return sqlContext.read.format("com.databricks.spark.csv")
		.option("header", "false")
		.schema(schema)
		.load(path)
}

def saveCsv(df: DataFrame, path: String) {
	df.write.format("com.databricks.spark.csv")
		.option("header", "false")
		.save(path)
}
